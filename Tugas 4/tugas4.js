//SOAL 1
console.log('SOAL 1');
console.log(' ');
console.log('LOOPING PERTAMA');
var a = 0;
while(a < 21) {
    console.log(a + ' - I Love Coding');
    a+=2;
}

console.log(' ');

console.log('LOOPING KEDUA');
var b = 20;
while(b > 0) {
    console.log(b + ' - I Will Become a Frontend Developer');
    b-=2;
}

console.log(' ');
//SOAL 2
console.log('Soal 2');
console.log(' ');
var angka =''
for (var i = 0; i <= 20; i++) {
    if (i % 2 == 0) {
      console.log(i + ' - Santai ');
    } else if (i%2 != 0 && i%3 == 0) {
      console.log(i + ' - I Love Coding ');
    } else if (i%2 != 0 && i%3 != 0) {
      console.log(i + ' - Berkualitas ');
    }
}

console.log(' ');
//SOAL 3
console.log('Soal 3');
console.log(' ');
for(var i=1; i<=7; i++){
    console.log("*".repeat(i));
 }

 console.log(' ');
 //SOAL 4
console.log('Soal 4');
console.log(' ');
var kalimat = "saya sangat senang belajar javascript"
var dipisah = kalimat.split(" ")
console.log(dipisah)

console.log(' ');
//SOAL 5
console.log('Soal 5');
console.log(' ');
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var listbuah = daftarBuah.sort();
for (i = 0;  i < listbuah.length; i++){
    console.log(listbuah[i]);
   }
