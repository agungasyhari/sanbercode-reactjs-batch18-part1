//Soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

//Jawaban 1
console.log(kataPertama.concat(" ").concat(kataKedua).concat(" ").concat(kataKetiga).concat(" ").concat(kataKeempat)); 

//Soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

//Jawaban 2
// var strkataPertama = String(kataPertama);
// var strkataKedua = String(kataKedua);
// var strkataKetiga = String(kataKetiga);
// var strkataKeempat = String(kataKeempat);

console.log(parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat));

//Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 19);
var kataKeempat = kalimat.substring(19, 25);
var kataKelima = kalimat.substring(25, 32);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

//Jawaban 3


//Soal 4
var nilai = 75;
if (nilai>=80){
    console.log("Indeks A")
} else if (nilai>=70 && nilai<80) {
    console.log("Indeks B")
} else if (nilai>=60 && nilai<70) {
    console.log("Indeks C")
} else if (nilai>=50 && nilai<60) {
    console.log("Indeks D")
} else if (nilai<50) {
    console.log("Indeks E")
}


//Jawaban 4
console.log(nilai);


//Soal 5
var tanggal = 04;
var bulan = 2;
var tahun = 1994;
var bulanlahir = "";
switch(bulan) {
  case 2: bulanlahir = "Februari"; break;
}


//Jawaban 5
console.log(tanggal + " " + bulanlahir + " " + tahun);
