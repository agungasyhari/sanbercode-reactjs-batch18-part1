//SOAL 1

//RELEASE 0

class Animal {
    constructor (name, legs, cold_blooded){
        this.nama = name;
        this.kaki = legs;
        this.darah = cold_blooded
    }
    get name (){
        return this.nama;
    }
    get legs (){
        return this.kaki;
    }
    get cold_blooded () {
        return this.darah;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs = 4 ) // 4
console.log(sheep.cold_blooded = "false") // false


//RELEASE 1

class Ape extends Animal {
    constructor(name, legs, cold_blooded, sound){
        super(name, legs, cold_blooded) ;
        this.suara = sound;
    }
    get sound (){
        return this.suara;
    }
}


var sungokong = new Ape("kera sakti")
console.log(sungokong.name)
console.log(sungokong.suara = "Auooo")
// sungokong.suara() // "Auooo"

class Frog extends Animal {
    constructor(name, legs, cold_blooded, jump){
        super(name, legs, cold_blooded) ;
        this.lompat = jump;
    }
    get jump (){
        return this.lompat;
    }
}

 
var kodok = new Frog("buduk")
console.log(kodok.name)
console.log(kodok.lompat = "hop hop")
// kodok.jump() // "hop hop" 


// //SOAL 2


class Clock {
    constructor({ template }) {
      this.template = template;
    }
  
    render() {
      var date = new Date();
      var hours = date.getHours();
      var mins = date.getMinutes();
      var secs = date.getSeconds();
  
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    }
  }

  let clock = new Clock({template: 'h:m:s'});
  clock.start();

