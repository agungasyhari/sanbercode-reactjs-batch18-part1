//SOAL 1
function halo() {
    return "Halo Sanbers!"
  }
   
console.log(halo());
  

//SOAL 2

function kalikan (num1, num2) {
    return num1 * num2
}


var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)


//SOAL 3
/* 
    Tulis kode function di sini
*/
function introduce (name, age, address, hobby) {
    return ("Nama saya " + name + ", umur saya " + age + " Tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby)
}
 
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!" 



//SOAL 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var daftarorang = {
    nama : "Asep",
    "jenis kelamin" : "laki-laki",
    hobi : "baca buku",
    tahun_lahir : 1992
}

console.log(daftarorang);

//SOAL 5
var buah = [
    {
    nama: "strawberry",
    warna: "merah",
    "ada bijinya" : "tidak",
    harga: 9000 
    },
    {
    nama: "jeruk",
    warna: "oranye",
    "ada bijinya" : "ada",
    harga: 8000
    },
    {
    nama: "Semangka",
    warna: "Hijau & Merah",
    "ada bijinya" : "ada",
    harga: 10000 
    },
    {
    nama: "Pisang",
    warna: "Kuning",
    "ada bijinya" : "tidak",
    harga: 5000
    }
]

console.log(buah[0]);


//SOAL 6
var dataFilm = [];
function tambahfilm (nama, durasi,genre,tahun){
    dataFilm.push({"nama":nama, "durasi":durasi, "genre":genre, "tahun":tahun})
}

tambahfilm("spiderman ngesot", "1 Jam", "horor", "1999");
tambahfilm("fantastic for 4", "1 Jam", "action", "2003");
console.log(dataFilm);